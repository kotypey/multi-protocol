#include <iostream>
#include "ua_lokha_multiprotocol_util_NativeUtils.h"

int main() {
    std::cout << "hello world";
}

void remapChunk1_9to1_8(const long *blockData, const int *palette, uint bitsPerBlock, signed char *desc) {
    uint singleValMask = (1u << bitsPerBlock) - 1;
    int pos = 0;
    for (uint block = 0; block < 4096; block++) {
        uint bitStartIndex = block * bitsPerBlock;
        uint arrStartIndex = bitStartIndex >> 6u;
        uint arrEndIndex = ((bitStartIndex + bitsPerBlock) - 1) >> 6u;
        uint localStartBitIndex = bitStartIndex & 63u;
        uint paletteIndex;
        if (arrStartIndex == arrEndIndex) {
            paletteIndex = (int) (((ulong) blockData[arrStartIndex] >> localStartBitIndex) & singleValMask);
        } else {
            paletteIndex = (int) ((((ulong) blockData[arrStartIndex] >> localStartBitIndex) |
                                   ((ulong) blockData[arrEndIndex] << (64 - localStartBitIndex))) & singleValMask);
        }
        uint blockState = palette[paletteIndex];
        desc[pos++] = blockState;
        desc[pos++] = blockState >> 8u;
    }
}

JNIEXPORT void JNICALL Java_ua_lokha_multiprotocol_util_NativeUtils_remapChunk1_19to1_18_00024native
        (JNIEnv *env, jclass, jlongArray blockData, jintArray palette, jshort bitsPerBlock, jbyteArray desc) {
    jlong *blockDataArray = env->GetLongArrayElements(blockData, nullptr);
    jint *platteArray = env->GetIntArrayElements(palette, nullptr);
    jbyte *descArray = env->GetByteArrayElements(desc, nullptr);
    remapChunk1_9to1_8(blockDataArray, platteArray, bitsPerBlock, descArray);
    env->ReleaseByteArrayElements(desc, descArray, 0);
}