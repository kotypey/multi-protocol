package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class Rotation implements Type, MetadataType {
    private float pitch;
    private float yaw;
    private float roll;

    @Override
    public void read(ByteBuf buf, Version version) {
        buf.writeFloat(pitch);
        buf.writeFloat(yaw);
        buf.writeFloat(roll);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        pitch = buf.readFloat();
        yaw = buf.readFloat();
        roll = buf.readFloat();
    }
}
