package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class Chat implements Type, MetadataType {
    private String json;

    @Override
    public void read(ByteBuf buf, Version version) {
        json = PacketUtils.readString(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeString(json, buf);
    }
}
