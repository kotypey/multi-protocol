package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

import java.util.UUID;

@Data
public class OptUuid implements Type, MetadataType {
    private boolean present;
    private UUID uuid;

    @Override
    public void read(ByteBuf buf, Version version) {
        present = buf.readBoolean();
        if (present) {
            uuid = PacketUtils.readUUID(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(present);
        if (present) {
            PacketUtils.writeUUID(uuid, buf);
        }
    }
}
