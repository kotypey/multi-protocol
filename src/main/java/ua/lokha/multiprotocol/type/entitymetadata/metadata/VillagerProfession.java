package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.VarIntType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
//TODO: неизвестно, нужно ли будет VilagerData как MetadataType объявлять
public class VillagerProfession implements Metadata {

    private int type;
    private int profession;
    private int level;

    @Override
    public void read(ByteBuf buf, Version version) {

        if(version.isAfter(Version.MINECRAFT_1_13_2)) {
            type = PacketUtils.readVarInt(buf);
            profession = PacketUtils.readVarInt(buf);
            level = PacketUtils.readVarInt(buf);
        } else if(version.isBefore(Version.MINECRAFT_1_9)) {
            profession = buf.readInt();
        } else
            profession = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {

        if(version.isAfter(Version.MINECRAFT_1_13_2)) {
            PacketUtils.writeVarInt(type, buf);
            PacketUtils.writeVarInt(profession, buf);
            PacketUtils.writeVarInt(level, buf);
        } else if(version.isBefore(Version.MINECRAFT_1_9)) {
            buf.writeInt(profession);
        } else
            PacketUtils.writeVarInt(profession, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return VarIntType.class;
    }
}
