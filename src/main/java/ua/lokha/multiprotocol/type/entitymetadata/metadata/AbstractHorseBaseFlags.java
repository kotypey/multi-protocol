package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.ByteType;
import ua.lokha.multiprotocol.type.IntType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class AbstractHorseBaseFlags implements Metadata {
    private int value;

    @Override
    public void read(ByteBuf buf, Version version) {
        value = version.isBefore(Version.MINECRAFT_1_9) ? buf.readInt() : buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.readUnsignedInt();
        if(version.isBefore(Version.MINECRAFT_1_9))
            buf.writeInt(value);
        else
            buf.writeByte(value);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            return IntType.class;

        return ByteType.class;
    }
}
