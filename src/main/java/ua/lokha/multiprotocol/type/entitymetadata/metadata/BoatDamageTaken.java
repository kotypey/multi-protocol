package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.FloatType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class BoatDamageTaken implements Metadata {
    private float damage;

    @Override
    public void read(ByteBuf buf, Version version) {
        damage = buf.readFloat();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeFloat(damage);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return FloatType.class;
    }
}
