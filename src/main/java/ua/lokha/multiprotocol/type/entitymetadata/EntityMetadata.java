package ua.lokha.multiprotocol.type.entitymetadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.EntityType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
@Log
public class EntityMetadata implements Type {
    private MetadataProtocol protocol;
    private List<Metadata> metadata = Collections.emptyList();

    public EntityMetadata(@NonNull EntityType mobType) {
        this.protocol = MetadataProtocol.getByEntity(mobType);
    }

    public EntityMetadata(@NonNull MetadataProtocol protocol) {
        this.protocol = protocol;
    }

    @Override
    public void read(ByteBuf buf, Version version) {
        metadata = new ArrayList<>();
        int endIndex = getEndIndex(version);
        while (true) {
            int index;
            int typeId;
            if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
                index = buf.readUnsignedByte();
                if (index == endIndex) {
                    break;
                }
                typeId = readVarInt(buf);
            } else {
                byte typeAndIndex = buf.readByte();
                typeId = (typeAndIndex & 224) >> 5;
                index = typeAndIndex & 31;
                if (index == endIndex) {
                    break;
                }
            }
            Metadata metadata = protocol.create(index, version);
            if (metadata == null) {
                log.info("metadata not found by index " + index + ", protocol " + protocol + ", version " + version);
                // skip bytes
                Type type = MetadataProtocol.TYPE_PROTOCOL.create(typeId, version);
                type.read(buf, version);
            } else {
                try {
                    metadata.read(buf, version);
                    this.metadata.add(metadata);
                } catch (Exception e) {
                    throw new RuntimeException("error read " + metadata + ", index " + index + ", version " + version + ", protocol " + protocol, e);
                }
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        for (Metadata item : metadata) {
            int index = protocol.getId(item.getClass(), version);
            if (index == -1) {
                log.info("remap entity metadata index not found " + protocol + " by version " +
                        version + ", metadata " + item);
            } else {
                int typeId = MetadataProtocol.TYPE_PROTOCOL.getId(item.getTypeClass(version), version);
                if (typeId == -1) {
                    log.info("remap entity metadata type not found " + protocol + " by version " +
                            version + ", type " + item.getTypeClass(version) + ", metadata " + item);
                } else {
                    log.info("remap entity metadata " + protocol + ", metadata " +
                            protocol.getId(item.getClass(), Version.getServerVersion()) + " -> " + index + " " + item);
                    if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
                        buf.writeByte(index);
                        writeVarInt(typeId, buf);
                    } else {
                        int typeAndIndex = (typeId << 5 | index & 31) & 255;
                        buf.writeByte(typeAndIndex);
                    }
                    item.write(buf, version);
                }
            }
        }

        buf.writeByte(getEndIndex(version));
    }

    public static int getEndIndex(Version version) {
        return version.isAfterOrEq(Version.MINECRAFT_1_9) ? 0xff : 0x7f;
    }
}
