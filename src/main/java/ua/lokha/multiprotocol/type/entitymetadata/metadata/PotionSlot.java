package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.Slot;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
public class PotionSlot extends Slot implements Metadata {
    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        super.read(buf, version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        super.write(buf, version);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        return Slot.class;
    }
}
