package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.BooleanType;
import ua.lokha.multiprotocol.type.ByteType;
import ua.lokha.multiprotocol.type.IntType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class GuardianRetractingSpikes implements Metadata {
    private boolean retractingSpikes;

    @Override
    public void read(ByteBuf buf, Version version) {
        retractingSpikes = buf.readBoolean(); //TODO: remap
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            buf.writeInt(retractingSpikes ? 2 : 0);
        else if (version.isBefore(Version.MINECRAFT_1_10)) {
            buf.writeByte(retractingSpikes ? 2 : 0);
        } else
            buf.writeBoolean(retractingSpikes);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            return IntType.class;
        else if(version.isBefore(Version.MINECRAFT_1_10))
            return ByteType.class;

        return BooleanType.class;
    }
}
