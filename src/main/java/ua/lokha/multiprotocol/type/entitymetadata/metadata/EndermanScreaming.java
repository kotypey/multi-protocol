package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.BooleanType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class EndermanScreaming implements Metadata {
    private boolean screaming;

    @Override
    public void read(ByteBuf buf, Version version) {
        screaming = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(screaming);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return BooleanType.class;
    }
}
