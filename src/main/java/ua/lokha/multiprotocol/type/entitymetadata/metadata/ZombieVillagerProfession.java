package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.VarIntType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
/**
 * @see VillagerProfession, тут такая же ситуация
 */
public class ZombieVillagerProfession implements Metadata {

    private int type;
    private int profession;
    private int level;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_11)) {
            throw new UnsupportedVersionException(version);
        }
        if(version.isAfter(Version.MINECRAFT_1_13_2)) {
            type =  PacketUtils.readVarInt(buf);
            profession =  PacketUtils.readVarInt(buf);
            level =  PacketUtils.readVarInt(buf);
        } else
            profession = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_11)) {
            throw new UnsupportedVersionException(version);
        }
        if(version.isAfter(Version.MINECRAFT_1_13_2)) {
            PacketUtils.writeVarInt(type, buf);
            PacketUtils.writeVarInt(profession, buf);
            PacketUtils.writeVarInt(level, buf);
        } else
            PacketUtils.writeVarInt(profession, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if (version.isBefore(Version.MINECRAFT_1_11)) {
            throw new UnsupportedVersionException(version);
        }
        return VarIntType.class;
    }
}
