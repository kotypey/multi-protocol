package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.BooleanType;
import ua.lokha.multiprotocol.type.ByteType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class MinecartFurnacePowered implements Metadata {
    private boolean powered;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isAfter(Version.MINECRAFT_1_14)) {
            throw new UnsupportedVersionException(version);
        }
        powered = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isAfter(Version.MINECRAFT_1_14)) {
            throw new UnsupportedVersionException(version);
        }
        buf.writeBoolean(powered);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if (version.isAfter(Version.MINECRAFT_1_14)) {
            throw new UnsupportedVersionException(version);
        }
        if(version.isBefore(Version.MINECRAFT_1_9))
            return ByteType.class;

        return BooleanType.class;
    }
}
