package ua.lokha.multiprotocol.type.entitymetadata;

public interface MetadataCreator <T extends Metadata> {
   T create();
}
