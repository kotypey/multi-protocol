package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.type.nbt.Tag;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Log
@Data
public class Slot implements Type, MetadataType {
    private String itemType;
    private byte count;
    private Tag tag;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_13_2)) {
            boolean present = buf.readBoolean();
            if (present) {
                int id = readVarInt(buf);
                itemType = ItemProtocol.ITEM_PROTOCOL.getValue(id, version);
                if (itemType == null) {
                    log.warning("itemType not found by id " + id + " and version " + version);
                    itemType = ItemProtocol.STONE;
                }
                count = buf.readByte();
                tag = Tag.readNamedTag(buf);
            }
        } else {
            int id = buf.readShort();
            if (id != -1) {
                count = buf.readByte();
                if (version.isBeforeOrEq(Version.MINECRAFT_1_12_2)) {
                    short damage = buf.readShort();
                    itemType = ItemProtocol.ITEM_PROTOCOL.getValue(id << 4 | damage , version);
                } else {
                    itemType = ItemProtocol.ITEM_PROTOCOL.getValue(id, version);
                }
                if (itemType == null) {
                    log.warning("itemType not found by id " + id + " and version " + version);
                    itemType = ItemProtocol.STONE;
                }
                tag = Tag.readNamedTag(buf);
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_13_2)) {
            buf.writeBoolean(itemType != null);
            if (itemType != null) {
                int id = ItemProtocol.ITEM_PROTOCOL.getId(itemType, version);
                if (id == -1) {
                    log.warning("id not found by itemType " + itemType + " and version " + version);
                    id = ItemProtocol.ITEM_PROTOCOL.getId("stone", version);
                }
                writeVarInt(id, buf);
                buf.writeByte(count);
                Tag.writeNamedTag(tag, buf);
            }
        } else {
            if (itemType == null) {
                buf.writeShort(-1);
            } else {
                int id = ItemProtocol.ITEM_PROTOCOL.getId(itemType, version);
                if (id == -1) {
                    log.warning("id not found by itemType " + itemType + " and version " + version);
                    id = ItemProtocol.ITEM_PROTOCOL.getId("stone", version);
                }
                if (version.isAfterOrEq(Version.MINECRAFT_1_13)) {
                    buf.writeShort(id);
                    buf.writeByte(count);
                } else {
                    byte damage = (byte) (id & 0xF);
                    int mainId = id >> 4;

                    buf.writeShort(mainId);
                    buf.writeByte(count);
                    buf.writeShort(damage);
                }
                Tag.writeNamedTag(tag, buf);
            }
        }
    }
}
