package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class StringType implements Type, MetadataType {
    private String value;

    @Override
    public void read(ByteBuf buf, Version version) {
        value = PacketUtils.readString(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeString(value, buf);
    }
}
