package ua.lokha.multiprotocol.type.nbt;

/**
 * Copyright Mojang AB.
 * 
 * Don't do evil.
 */

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ListTag<T extends Tag> extends Tag {
    private byte type;
    private List<T> list = Collections.emptyList();

    public ListTag() {
        super("");
    }

    public ListTag(String name) {
        super(name);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void read(ByteBuf dis) {
        type = dis.readByte();
        int size = dis.readInt();

        list = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            Tag tag = Tag.createTag(type, null);
            tag.read(dis);
            list.add((T) tag);
        }
    }

    @Override
    public void write(ByteBuf dos) {
        if (list.size() > 0) type = list.get(0).getId();
        else type = 1;

        dos.writeByte(type);
        dos.writeInt(list.size());
        for (int i = 0; i < list.size(); i++)
            list.get(i).write(dos);
    }

    public void add(T tag) {
        if (list == Collections.EMPTY_LIST) {
            list = new ArrayList<>(4);
        }
        type = tag.getId();
        list.add(tag);
    }

    public T get(int index) {
        return list.get(index);
    }

    public int size() {
        return list.size();
    }

    @Override
    public byte getId() {
        return TAG_List;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Tag copy() {
        ListTag<T> res = new ListTag<T>(getName());
        res.type = type;
        for (T t : list) {
            T copy = (T) t.copy();
            res.add(copy);
        }
        return res;
    }
}
