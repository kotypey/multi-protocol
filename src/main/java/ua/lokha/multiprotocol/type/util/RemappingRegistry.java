package ua.lokha.multiprotocol.type.util;


import ua.lokha.multiprotocol.Version;

import java.util.EnumMap;

public abstract class RemappingRegistry<T extends RemappingTable> {

	protected final EnumMap<Version, T> registry = new EnumMap<>(Version.class);

	public RemappingRegistry() {
		clear();
	}

	public void clear() {
		for (Version version : Version.getVersions()) {
			registry.put(version, createTable());
		}
	}

	public T getTable(Version version) {
		return registry.get(version);
	}

	protected abstract T createTable();

	public abstract static class IdRemappingRegistry<T extends RemappingTable.IdRemappingTable> extends RemappingRegistry<T> {

		public void registerRemapEntry(int from, int to, Version... versions) {
			for (Version version : versions) {
				getTable(version).setRemap(from, to);
			}
		}

	}

	public abstract static class EnumRemappingRegistry<T extends Enum<T>, R extends RemappingTable.EnumRemappingTable<T>> extends RemappingRegistry<R> {

		public void registerRemapEntry(T from, T to, Version... versions) {
			for (Version version : versions) {
				getTable(version).setRemap(from, to);
			}
		}

	}

	public abstract static class GenericRemappingRegistry<T, R extends RemappingTable.GenericRemappingTable<T>> extends RemappingRegistry<R> {

		public void registerRemapEntry(T from, T to, Version... versions) {
			for (Version version : versions) {
				getTable(version).setRemap(from, to);
			}
		}

	}

}
