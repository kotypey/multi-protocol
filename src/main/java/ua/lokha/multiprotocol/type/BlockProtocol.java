package ua.lokha.multiprotocol.type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Id2ObjectVersionMapper;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.ResourceUtils;

import java.util.*;
import java.util.stream.Collectors;

@Log
public class BlockProtocol {

    public static String AIR = "air";
    public static String STONE = "stone";
    public static Id2ObjectVersionMapper<String> BLOCK_PROTOCOL;

    /**
     * В чанках 1.14 используется в расчетах, используется boolean[] для оптимизации
     */
    public static Map<Version, boolean[]> MOTION_BLOCKING;

    static {
        BLOCK_PROTOCOL = Id2ObjectVersionMapper.loadFromResourceStringValue("block-protocol.json");

        loadMotionBlocks();
    }

    private static void loadMotionBlocks() {
        JsonArray motionBlocking = ResourceUtils.GSON.fromJson(Objects.requireNonNull(
                ResourceUtils.getAsBufferedReader("motion-blocking.json")), JsonArray.class);

        List<String> blocks = new ArrayList<>(motionBlocking.size());
        for (JsonElement element : motionBlocking) {
            blocks.add(element.getAsString());
        }

        MOTION_BLOCKING = new EnumMap<>(Version.class);
        for (Version version : Version.getVersions()) {
            List<Integer> ids = blocks.stream()
                    .map(value -> BLOCK_PROTOCOL.getId(value, version))
                    .filter(value -> value != -1)
                    .collect(Collectors.toList());

            int maxId = ids.stream().mapToInt(value -> value).max().orElse(0);
            if (maxId > 20000) {
                throw new IllegalStateException("maxId " + maxId + " > 20000, оптимизация была расчитана на малый " +
                        "предел id, повысить лимит должен кодер вручную");
            }

            boolean[] containsIds = new boolean[maxId + 1];
            for (Integer id : ids) {
                containsIds[id] = true;
            }

            MOTION_BLOCKING.put(version, containsIds);
        }
    }

    /**
     * Преобразовать указанный id блока в id в другой версии
     *
     * @param id        айди блока
     * @param version   версия, в который был передан id
     * @param toVersion версия, в которую нужно преобразовать id
     * @return id этого же блока, только из другой версии, которая была передана аргументом toVersion
     */
    public static int mapBlockId(int id, Version version, Version toVersion) {
        if (id == 0) { // чаще всего проверяется, вынесего для оптимизации
            return 0;
        }
        String blockType = BLOCK_PROTOCOL.getValue(id, version);
        if (blockType != null) {
            int newId = BLOCK_PROTOCOL.getId(blockType, toVersion);
            if (newId != -1) {
                id = newId;
            } else {
                log.warning("remap id not found by blockType " + blockType + " and version " + toVersion);
                id = 0;
            }
        } else {
            log.warning("blockType not found by id " + id + " and version " + version);
            id = 0;
        }
        return id;
    }
}
