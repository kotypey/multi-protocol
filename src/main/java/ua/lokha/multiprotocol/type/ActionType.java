package ua.lokha.multiprotocol.type;

public enum ActionType {
    START_SNEAK,
    STOP_SNEAK,
    LEAVE_BED,
    START_SPRINT,
    STOP_SPRINT,
    JUMP_HORSE,
    START_JUMP_HORSE,
    STOP_JUMP_HORSE,
    OPEN_HORSE,
    START_FLY_ELYTRA
}
