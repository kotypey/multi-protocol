package ua.lokha.multiprotocol;

import java.util.UUID;

/**
 * Сюда вынесены все методы, которые формируют соглашения по remap'у.
 * Например, правила конвертации uuid -> int и наборот.
 */
public class RemapConventions {

    /**
     * Получить uuid для entity с указанным id
     */
    public static UUID entityIdToUUID(int entityId) {
        return new UUID(entityId, 0);
    }
}
