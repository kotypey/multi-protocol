package ua.lokha.multiprotocol;

import lombok.Setter;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestProtocolCommand implements CommandExecutor, TabExecutor {

	private List<String> args0 = Arrays.asList("particle", "item", "entity", "block", "sound", "mount", "unmount");

	@Setter
	private TestProtocolWhoKick whoKick;

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		if(args.length == 1) {
			return this.args0;
		}

		if(args.length == 2) {
			return Collections.singletonList("print");
		}
		return Collections.emptyList();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length == 0) {
			sender.sendMessage("§e============[Test Protocol]============\n" +
					"§4/testprotocol particle <print> §7- проверить частицы\n" +
					"§4/testprotocol item <print> §7- проверить предметы в инветаре\n" +
					"§4/testprotocol entity <print> §7- проверить мобов\n" +
					"§4/testprotocol block <print> §7- проверить блоки\n" +
					"§4/testprotocol sound <print> §7- проверить звуки\n" +
					"§4/testprotocol unmount <print> §7- снять блишайшего моба с транспорта и убрать пассажиров\n" +
					"§4/testprotocol mount <print> §7- посадить ближаших мобов друг на друга\n" +
					"§4/testprotocol whokick §7- включить/выключить систему определять проблемных пакетов\n" +
					"§7Эта команда позволяет проверить, не кикает ли игрока из-за разных вещей с других версий игры.\n" +
					"§7Если в конце команды дописать print, то будет логирование");
			return false;
		}

		Print print = args.length > 1 && args[1].equalsIgnoreCase("print") ? (playerTest, name) -> {
			Bukkit.getLogger().info("Игрок " + playerTest.getName() + " тестит " + name);
			playerTest.sendMessage("§eТестим " + name);
		} : (userTest, name) -> {
		};

		if(args[0].equals("whokick")) {
			if (whoKick != null) {
				whoKick.stop();
				whoKick = null;
				sender.sendMessage("§aОстановили whokick.");
				return true;
			}

			sender.sendMessage("§aЗапускаем whokick, все логи будут в консоли сервера.");
			whoKick = new TestProtocolWhoKick();
			whoKick.start();
			return true;
		}
		if(args[0].equals("unmount")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only for player.");
				return false;
			}
			Player player = (Player) sender;

			Location loc = player.getLocation();
			Entity entity = player.getWorld()
					.getEntities().stream()
					.filter(it -> !it.getType().equals(EntityType.PLAYER))
					.min(Comparator.comparingDouble(value -> value.getLocation().distanceSquared(loc)))
					.orElse(null);

			if (entity == null) {
				player.sendMessage("§cРядом мобов нет.");
				return true;
			}

			entity.getPassengers().forEach(entity::removePassenger);
			entity.leaveVehicle();

			player.sendMessage("§eСняли с моба " + entity + " пассажиров и сами его сняли с транспорта.");
			return true;
		}

		if(args[0].equals("mount")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only for player.");
				return false;
			}
			Player player = (Player) sender;

			Location loc = player.getLocation();
			List<Entity> entities = player.getWorld()
					.getEntities().stream()
					.filter(it -> !it.getType().equals(EntityType.PLAYER))
					.sorted(Comparator.comparingDouble(value -> value.getLocation().distanceSquared(loc)))
					.limit(2).collect(Collectors.toList());

			if (entities.isEmpty()) {
				player.sendMessage("§cРядом нет двух мобов.");
				return true;
			}

			for (Entity entity : entities) {
				entity.getPassengers().forEach(entity::removePassenger);
				entity.leaveVehicle();
			}

			entities.get(0).addPassenger(entities.get(1));

			player.sendMessage("§eПосадили " + entities.get(0) + " на " + entities.get(1));
			return true;
		}

		if(args[0].equals("particle")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only for player.");
				return false;
			}
			Player player = (Player) sender;

			player.sendMessage("§eНачало теста.");

			Iterator<Particle> iterator = Stream.of(Particle.values()).iterator();
			new BukkitRunnable() {
				@Override
				public void run() {
					if (!iterator.hasNext()) {
						this.cancel();
						return;
					}
					Particle next = iterator.next();
					print.print(player, next.name());
					player.spawnParticle(next, player.getLocation(), 1, 0, 0, 0);
					if(!iterator.hasNext()) {
						player.sendMessage("§aКонец теста. Если вас не кикнуло, значит все хорошо.");
					}
				}
			}.runTaskTimer(Main.getInstance(), 1, 1);
			return true;
		}

		if(args[0].equals("item")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only for player.");
				return false;
			}
			Player player = (Player) sender;

			player.sendMessage("§eНачало теста.");

			PlayerInventory inventory = player.getInventory();
			Iterator<Material> iterator = Stream.of(Material.values()).iterator();
			new BukkitRunnable() {
				@Override
				public void run() {
					if (!iterator.hasNext()) {
						this.cancel();
						return;
					}
					Material next = iterator.next();
					print.print(player, next.name());
					inventory.setItem(inventory.getHeldItemSlot(), new ItemStack(next));
					if(!iterator.hasNext()) {
						player.sendMessage("§aКонец теста. Если вас не кикнуло, значит все хорошо.");
					}
				}
			}.runTaskTimer(Main.getInstance(), 1, 1);
			return true;
		}

		if(args[0].equals("entity")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only for player.");
				return false;
			}
			Player player = (Player) sender;

			player.sendMessage("§eНачало теста.");

			Iterator<EntityType> iterator = Stream.of(EntityType.values()).iterator();
			final AtomicReference<Entity> entity = new AtomicReference<>();
			new BukkitRunnable() {
				@Override
				public void run() {
					if (!iterator.hasNext()) {
						this.cancel();
						return;
					}
					EntityType next = iterator.next();
					if(entity.get() != null && entity.get().isValid()) {
						entity.get().remove();
					}
					print.print(player, next.name());
					try {
						Entity spawnEntity = player.getWorld().spawnEntity(player.getLocation(), next);
						if(spawnEntity != null) {
							entity.set(spawnEntity);
						}
					} catch(Exception ignore) {}

					if(!iterator.hasNext()) {
						player.sendMessage("§aКонец теста. Если вас не кикнуло, значит все хорошо.");
					}
				}
			}.runTaskTimer(Main.getInstance(), 1, 1);
			return true;
		}

		if(args[0].equals("block")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only for player.");
				return false;
			}
			Player player = (Player) sender;

			player.sendMessage("§eНачало теста.");

			Iterator<Material> iterator = Stream.of(Material.values()).iterator();
			Block block = player.getLocation().getBlock().getRelative(BlockFace.EAST);
			new BukkitRunnable() {
				@Override
				public void run() {
					if (!iterator.hasNext()) {
						this.cancel();
						return;
					}
					Material next = iterator.next();
					if(next.isBlock()) {
						print.print(player, next.name());
						block.setType(next);
					}
					if(!iterator.hasNext()) {
						block.setType(Material.AIR);
						player.sendMessage("§aКонец теста. Если вас не кикнуло, значит все хорошо.");
					}
				}
			}.runTaskTimer(Main.getInstance(), 1, 1);
			return true;
		}

		if(args[0].equals("sound")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("Only for player.");
				return false;
			}
			Player player = (Player) sender;

			player.sendMessage("§eНачало теста.");

			Iterator<Sound> iterator = Stream.of(Sound.values()).iterator();
			new BukkitRunnable() {
				@Override
				public void run() {
					if (!iterator.hasNext()) {
						this.cancel();
						return;
					}
					Sound next = iterator.next();
					print.print(player, next.name());
					player.playSound(player.getLocation(), next, 1, 1);
					if(!iterator.hasNext()) {
						player.sendMessage("§aКонец теста. Если вас не кикнуло, значит все хорошо.");
					}
				}
			}.runTaskTimer(Main.getInstance(), 1, 1);
			return true;
		}


		sender.sendMessage("§cАргумент команды не найден.");
		return false;
	}

	private interface Print {
		void print(Player player, String name);
	}
}
