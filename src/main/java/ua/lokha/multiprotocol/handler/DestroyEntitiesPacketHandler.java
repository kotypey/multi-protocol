package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.packet.play.DestroyEntitiesPacket;
import ua.lokha.multiprotocol.packet.play.SetPassengersPacket;

public class DestroyEntitiesPacketHandler implements PacketHandler<DestroyEntitiesPacket> {
    @Override
    public void handle(DestroyEntitiesPacket packet, Connection connection) {
        int[] entityIds = packet.getEntityIds();

        for (int entityId : entityIds) {
            connection.getViewEntities().remove(entityId);
            connection.clearEntityPassengers(entityId);
        }
    }
}
