package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.packet.play.SpawnExperienceOrbPacket;
import ua.lokha.multiprotocol.type.EntityType;

public class SpawnExperienceOrbPacketHandler implements PacketHandler<SpawnExperienceOrbPacket> {
    @Override
    public void handle(SpawnExperienceOrbPacket packet, Connection connection) {
        connection.getViewEntities().put(packet.getEntityId(), EntityType.EXPERIENCE_ORB);
    }
}
