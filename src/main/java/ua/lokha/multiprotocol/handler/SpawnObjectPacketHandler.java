package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.packet.play.SpawnObjectPacket;

public class SpawnObjectPacketHandler implements PacketHandler<SpawnObjectPacket> {
    @Override
    public void handle(SpawnObjectPacket packet, Connection connection) {
        connection.getViewEntities().put(packet.getEntityId(), packet.getType());
    }
}
