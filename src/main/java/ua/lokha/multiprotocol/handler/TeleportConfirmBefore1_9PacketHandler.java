package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.packet.play.OutPlayerPositionAndLookPacket;
import ua.lokha.multiprotocol.packet.play.TeleportConfirmPacket;

/**
 * Поддержка TeleportConfirm на версии 1.8.
 * На версии 1.8 этого пакета не было, а он должен отправляться в ответ на OutPlayerPositionAndLookPacket.
 */
public class TeleportConfirmBefore1_9PacketHandler implements PacketHandler<OutPlayerPositionAndLookPacket> {
    @Override
    public void handle(OutPlayerPositionAndLookPacket packet, Connection connection) {
        if (connection.getVersion().isBefore(Version.MINECRAFT_1_9)) {
            connection.receivePacket(new TeleportConfirmPacket(packet.getTeleportId()));
        }
    }
}
