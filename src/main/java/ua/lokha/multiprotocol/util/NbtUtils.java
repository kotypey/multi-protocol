package ua.lokha.multiprotocol.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.type.nbt.CompoundTag;

/**
 * Created by дартЪ on 05.02.2020
 */
@Log
public class NbtUtils {

    public static final Gson GSON = new GsonBuilder().create();

    public static String[] readSignLines(CompoundTag signTag) {
        String[] result = new String[4];
        for(int i = 0; i < 4; i++) {
            result[i] = signTag.getString("Text" + (i+1));
        }

        return result;
    }

    public static int getTileEntityId(String nbtId) {
        switch(nbtId) {
            case "minecraft:mob_spawner": {
                return 1;
            }
            case "minecraft:command_block": {
                return 2;
            }
            case "minecraft:beacon": {
                return 3;
            }
            case "minecraft:skull": {
                return 4;
            }
            case "minecraft:flower_pot": {
                return 5;
            }
            case "minecraft:banner": {
                return 6;
            }
            case "minecraft:structure_block": {
                return 7;
            }
            case "minecraft:end_gateway": {
                return 8;
            }
            case "minecraft:sign": {
                return 9;
            }
            case "minecraft:shulker_box": {
                return 10;
            }
            case "minecraft:bed": {
                return 11;
            }
        }
        return -1;
    }

    public static String fixJson(String input) {
        if ((input == null) || (input.equalsIgnoreCase("null"))) {
            input = "{\"text\":\"\"}";
        } else {
            if (((!input.startsWith("\"")) || (!input.endsWith("\""))) && ((!input.startsWith("{")) || (!input.endsWith("}")))) {
                return constructJson(input);
            }
            if ((input.startsWith("\"")) && (input.endsWith("\""))) {
                input = "{\"text\":" + input + "}";
            }
        }

        try {
            GSON.fromJson(input, JsonObject.class);
        } catch (Exception e) {
            log.severe("Ошибка парсинга JSON строки " + input);
            e.printStackTrace();
            return "{\"text\":\"\"}";
        }

        return input;
    }

    public static String constructJson(String input) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("text", input);
        return GSON.toJson(jsonObject);
    }
}
