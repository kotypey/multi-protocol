package ua.lokha.multiprotocol;

import io.netty.buffer.ByteBuf;

import java.util.Collections;
import java.util.List;

/**
 * Пакет
 */
public interface Packet extends Type {

    /**
     * Вызывается при remap пакета под нужную версию.
     * Если вам нужно полностью заменить этот пакет на другой пакет (или несколько других пакетов), то можно переопределить этот метод
     * и вернуть нужный список пакетов, которые будут обработаны вместо этого.
     */
    default List<Packet> map(Version version) {
        return Collections.singletonList(this);
    }

    /**
     * Этот метод нужен, чтобы более точно прочитать данные пакета, основываясь на соединении игрока, а не только версии
     */
    default void read(ByteBuf buf, Version version, Connection connection) {
        this.read(buf, version);
    }

    /**
     * Этот метод нужен, чтобы более точно записать данные пакета, основываясь на соединении игрока, а не только версии
     */
    default void write(ByteBuf buf, Version version, Connection connection) {
        this.write(buf, version);
    }
}
