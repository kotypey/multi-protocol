package ua.lokha.multiprotocol;

import lombok.extern.java.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Обработчик пакетов
 */
@Log
public class HandlerManager {

    private static HandlerManager instance = new HandlerManager();

    private Map<Class<? extends Packet>, List<PacketHandler>> handlers = new HashMap<>();

    /**
     * Зрегистрировать обработчик пакетов для обработки пакета
     * @return обработчик, который был передан вторым аргументом
     */
    public <T extends Packet> PacketHandler<T> registerHandler(Class<T> packetClass, PacketHandler<T> packetHandler) {
        handlers.computeIfAbsent(packetClass, key->new ArrayList<>()).add(packetHandler);
        return packetHandler;
    }

    /**
     * Отключить обработчик пакетов для всех зарегистрированных пакетов
     */
    public <T extends Packet> void unregisterHandler(PacketHandler<T> packetHandler) {
        for (List<PacketHandler> packetHandlers : handlers.values()) {
            packetHandlers.remove(packetHandler);
        }
    }

    public static HandlerManager getInstance() {
        return instance;
    }

    /**
     * Вызвать обработчики пакета
     */
    @SuppressWarnings("unchecked")
    public void callHandlers(Packet packet, Connection connection) {
        List<PacketHandler> handlers = this.handlers.get(packet.getClass());
        if (handlers != null) {
            for (PacketHandler handler : handlers) {
                try {
                    handler.handle(packet, connection);
                } catch (Exception e) {
                    log.severe("Ошибка обработки пакета " + packet + " обработчиком " + handler);
                    e.printStackTrace();
                }
            }
        }
    }
}
