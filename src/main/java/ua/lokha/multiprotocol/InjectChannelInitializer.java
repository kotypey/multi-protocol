package ua.lokha.multiprotocol;

import io.netty.channel.*;
import lombok.Getter;
import lombok.Lombok;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Обрабатывает входящие соединения.
 * Это нужно, чтобы мы могли добавлять свои handler'ы всем новым соединениям.
 */
public class InjectChannelInitializer extends ChannelInitializer {
    @Getter
    private static RemapOutboundHandler remapOutboundHandler = new RemapOutboundHandler();
    @Getter
    private static RemapInboundHandler remapInboundHandler = new RemapInboundHandler();

    private static Method ChannelInitializer_initChannel;
    private static Field ChannelHandlerAdapter_added;

    static {
        try {
            ChannelInitializer_initChannel = ChannelInitializer.class.getDeclaredMethod("initChannel", Channel.class);
            ChannelInitializer_initChannel.setAccessible(true);

            ChannelHandlerAdapter_added = ChannelHandlerAdapter.class.getDeclaredField("added");
            ChannelHandlerAdapter_added.setAccessible(true);
        } catch (NoSuchMethodException | NoSuchFieldException e) {
            //noinspection ThrowableNotThrown
            Lombok.sneakyThrow(e);
        }
    }
    // обработчик баккита, который мы заменяем
    private ChannelInitializer original;

    public InjectChannelInitializer(ChannelInitializer original) {
        this.original = original;
    }

    @Override
    protected void initChannel(Channel channel) throws Exception {
        ChannelInitializer_initChannel.invoke(original, channel);

        ChannelPipeline pipeline = channel.pipeline();
        ChannelHandler originalEncoder = pipeline.replace("encoder", "encoder", remapOutboundHandler);
        ChannelHandlerAdapter_added.set(originalEncoder, false);
        pipeline.addAfter("encoder", "original-encoder", originalEncoder);

        ChannelHandler originalDecoder = pipeline.replace("decoder", "decoder", remapInboundHandler);
        ChannelHandlerAdapter_added.set(originalDecoder, false);
        pipeline.addAfter("decoder", "original-decoder", originalDecoder);

        Connection connection = new Connection(channel);
        channel.attr(Connection.getAttributeKey()).set(connection);

        // начальный протокол handshake
        connection.setProtocol(Protocol.HANDSHAKE);

        // начальная версия, но уже после первого пакета она изменится на реальную версию клиента
        connection.setVersion(Version.getServerVersion());

    }
}
