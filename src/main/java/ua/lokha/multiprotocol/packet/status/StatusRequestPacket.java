package ua.lokha.multiprotocol.packet.status;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class StatusRequestPacket implements Packet {
    @Override
    public void read(ByteBuf buf, Version version) {

    }

    @Override
    public void write(ByteBuf buf, Version version) {

    }
}
