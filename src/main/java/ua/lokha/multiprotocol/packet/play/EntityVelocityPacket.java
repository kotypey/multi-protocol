package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class EntityVelocityPacket implements Packet {
    private int entityId;
    private short velocityX;
    private short velocityY;
    private short velocityZ;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        velocityX = buf.readShort();
        velocityY = buf.readShort();
        velocityZ = buf.readShort();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        buf.writeShort(velocityX);
        buf.writeShort(velocityY);
        buf.writeShort(velocityZ);
    }
}
