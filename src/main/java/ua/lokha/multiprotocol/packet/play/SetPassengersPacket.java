package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntListIterator;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@NoArgsConstructor
@Data
public class SetPassengersPacket implements Packet {

    private int entityId;
    private int[] passengers;

    //текущие пассажиры энтити (для метода map(...))
    private IntList currentPassengers;

    public SetPassengersPacket(int entityId, int[] passengers) {
        this.entityId = entityId;
        this.passengers = passengers;
    }

    @Override
    public void read(ByteBuf buf, Version version, Connection connection) {
        read(buf, version);

        currentPassengers = connection.getEntityPassengers(entityId);
    }

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }

        entityId = readVarInt(buf);
        passengers = readVarIntArray(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }

        writeVarInt(entityId, buf);
        writeVarIntArray(passengers, buf);
    }

    @Override
    public List<Packet> map(Version version) {
        if (version.isBeforeOrEq(Version.MINECRAFT_1_8)) {
            int currentPassengers = this.currentPassengers == null ? 0 : this.currentPassengers.size();
            int passengers = this.passengers.length > 0 ? 1 : 0;

            List<Packet> packets = new ArrayList<>(currentPassengers + passengers);

            //сначала удаляем старых энтити
            if (this.currentPassengers != null) {
                IntListIterator iterator = this.currentPassengers.iterator();
                if (iterator.hasNext()) {
                    packets.add(new AttachEntityPacket(iterator.nextInt(), -1));
                }
            }

            //отправляем AttachEntity с новыми
            if (passengers > 0) {
                packets.add(new AttachEntityPacket(this.passengers[0], entityId));
            }

            return packets;
        }

        return Collections.singletonList(this);
    }
}
