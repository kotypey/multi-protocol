package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class OutHeldItemChangePacket implements Packet {
    public byte slot;

    @Override
    public void read(ByteBuf buf, Version version) {
        slot = buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(slot);
    }
}
