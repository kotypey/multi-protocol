package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Position;

@Data
public class EffectPacket implements Packet {
    private int effectId;
    private int x;
    private int y;
    private int z;
    private int data;
    private boolean disableRelativeVolume;

    @Override
    public void read(ByteBuf buf, Version version) {
        effectId = buf.readInt();

        long position = buf.readLong();
        x = Position.getX(position, version);
        y = Position.getY(position, version);
        z = Position.getZ(position, version);

        data = buf.readInt();
        disableRelativeVolume = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeInt(effectId);
        buf.writeLong(Position.getPosition(x, y, z, version));
        buf.writeInt(data);
        buf.writeBoolean(disableRelativeVolume);
    }
}
