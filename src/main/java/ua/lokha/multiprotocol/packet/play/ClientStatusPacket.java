package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.util.PacketUtils;

import java.util.Collections;
import java.util.List;

@Data
public class ClientStatusPacket implements Packet {

    private int action;

    @Override
    public void read(ByteBuf buf, Version version) {
        action = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (action >= 2 && version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version, "action " + action + " >= 2 on 1.9+");
        }
        PacketUtils.writeVarInt(action, buf);
    }

    @Override
    public List<Packet> map(Version version) {
       if(action >= 2 && version.isAfterOrEq(Version.MINECRAFT_1_9))
           return Collections.emptyList();
       else
           return Collections.singletonList(this);
    }
}
