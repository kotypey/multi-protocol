package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class OpenWindowPacket implements Packet {

    private int windowId;
    private int windowType; //для регистра 1.14
    private String windowTypeStr;
    private String windowTitle;
    private int countSlots;
    private int entityId;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_14)) {
            windowId = buf.readUnsignedByte();
            windowTypeStr = PacketUtils.readString(buf);
            windowTitle = PacketUtils.readString(buf);
            countSlots = buf.readUnsignedByte();

            if(windowTypeStr.equals("EntityHorse"))
                entityId = buf.readInt();
        } else {
            windowId = PacketUtils.readVarInt(buf);
            windowType = PacketUtils.readVarInt(buf);
            windowTitle = PacketUtils.readString(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_14)) {
            buf.writeByte(windowId);
            PacketUtils.writeString(windowTypeStr, buf);
            PacketUtils.writeString(windowTitle, buf);
            buf.writeByte(countSlots);

            if(windowTypeStr.equals("EntityHorse"))
                buf.writeInt(entityId);
        } else {
            PacketUtils.writeVarInt(windowId, buf);
            PacketUtils.writeVarInt(windowType, buf);
            PacketUtils.writeString(windowTitle, buf);
        }
    }
}
