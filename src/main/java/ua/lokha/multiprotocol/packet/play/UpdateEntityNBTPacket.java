package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.nbt.Tag;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class UpdateEntityNBTPacket implements Packet {

    private int entityId;
    private Tag entityTag;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isAfterOrEq(Version.MINECRAFT_1_9))
            throw new UnsupportedVersionException(version);

        entityId = PacketUtils.readVarInt(buf);
        entityTag = Tag.readNamedTag(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isAfterOrEq(Version.MINECRAFT_1_9))
            throw new UnsupportedVersionException(version);

        PacketUtils.writeVarInt(entityId, buf);
        Tag.writeNamedTag(entityTag, buf);
    }
}
