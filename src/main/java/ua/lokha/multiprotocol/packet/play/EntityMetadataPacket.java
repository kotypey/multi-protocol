package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.EntityType;
import ua.lokha.multiprotocol.type.entitymetadata.EntityMetadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataProtocol;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
@Log
public class EntityMetadataPacket implements Packet {
    /**
     * Тип моба, не факт, что известно
     */
    private EntityType entityType;

    private int entityId;
    private EntityMetadata metadata;

    public EntityMetadataPacket(int entityId, EntityMetadata metadata) {
        this.entityId = entityId;
        this.metadata = metadata;
    }

    public EntityMetadataPacket(int entityId, EntityType entityType, EntityMetadata metadata) {
        this.entityType = entityType;
        this.entityId = entityId;
        this.metadata = metadata;
    }

    public EntityMetadataPacket() {
    }

    @Override
    public void read(ByteBuf buf, Version version, Connection connection) {
        entityId = readVarInt(buf);
        entityType = connection.getViewEntities().get(entityId);
        if (entityType == null) {
            log.warning("metadata is sent for an object that is not spawned for a player, entity id " + entityId + ", connection " + connection);
        } else {
            try {
                MetadataProtocol entity = MetadataProtocol.getByEntity(entityType);
                metadata = new EntityMetadata(entity);
                metadata.read(buf, version);
            } catch (IllegalStateException e) {
                log.warning("Metadata protocol not found by entity type " + entityType);
            }
        }
    }

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        if (entityType == null) {
            log.warning("metadata is sent for an object that is not spawned for a player, entity id " + entityId);
        } else {
            try {
                MetadataProtocol entity = MetadataProtocol.getByEntity(entityType);
                metadata = new EntityMetadata(entity);
                metadata.read(buf, version);
            } catch (IllegalStateException e) {
                log.warning("Metadata protocol not found by entity type " + entityType);
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        if (metadata != null) {
            metadata.write(buf, version);
        } else {
            buf.writeByte(EntityMetadata.getEndIndex(version));
        }
    }
}
