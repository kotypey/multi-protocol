package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

/**
 * Пакет, который не меняется от версии к версии
 */
public class NotRemapPacket implements Packet {
    private ByteBuf bytes;

    @Override
    public void read(ByteBuf buf, Version version) {
        bytes = buf.readBytes(buf.readableBytes());
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBytes(bytes);
    }
}