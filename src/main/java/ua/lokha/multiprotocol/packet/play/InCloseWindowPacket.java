package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class InCloseWindowPacket implements Packet {

    private short windowId;

    @Override
    public void read(ByteBuf buf, Version version) {
        windowId = buf.readUnsignedByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(windowId);
    }
}
