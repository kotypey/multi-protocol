package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;

import java.util.UUID;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

// todo сделать remap
@Data
public class BossBarPacket implements Packet {
    private UUID uuid;
    private int action;
    private String title;
    private float health;
    private int color;
    private int division;
    private byte flags;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        this.uuid = readUUID(buf);
        this.action = readVarInt(buf);
        switch(this.action) {
            case 0:
                this.title = readString(buf);
                this.health = buf.readFloat();
                this.color = readVarInt(buf);
                this.division = readVarInt(buf);
                this.flags = buf.readByte();
            case 1:
            default:
                break;
            case 2:
                this.health = buf.readFloat();
                break;
            case 3:
                this.title = readString(buf);
                break;
            case 4:
                this.color = readVarInt(buf);
                this.division = readVarInt(buf);
                break;
            case 5:
                this.flags = buf.readByte();
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        writeUUID(this.uuid, buf);
        writeVarInt(this.action, buf);
        switch(this.action) {
            case 0:
                writeString(this.title, buf);
                buf.writeFloat(this.health);
                writeVarInt(this.color, buf);
                writeVarInt(this.division, buf);
                buf.writeByte(this.flags);
            case 1:
            default:
                break;
            case 2:
                buf.writeFloat(this.health);
                break;
            case 3:
                writeString(this.title, buf);
                break;
            case 4:
                writeVarInt(this.color, buf);
                writeVarInt(this.division, buf);
                break;
            case 5:
                buf.writeByte(this.flags);
        }
    }
}
