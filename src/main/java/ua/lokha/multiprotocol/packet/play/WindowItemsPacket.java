package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Slot;

@Data
public class WindowItemsPacket implements Packet {

    private short windowId;
    private short updateCount;
    private Slot[] items;

    @Override
    public void read(ByteBuf buf, Version version) {
        windowId = buf.readUnsignedByte();
        updateCount = buf.readShort();
        items = new Slot[updateCount];

        for(int i = 0; i < updateCount; i++) {
            items[i] = new Slot();
            items[i].read(buf, version);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(windowId);
        buf.writeShort(updateCount);

        for(Slot item: items) {
            item.write(buf, version);
        }
    }
}
