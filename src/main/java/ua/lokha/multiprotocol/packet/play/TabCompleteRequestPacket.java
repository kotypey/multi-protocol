package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Position;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class TabCompleteRequestPacket implements Packet {

    private int transactionId;
    private String cursor;
    private boolean assumeCommand;
    private boolean hasPositon;
    private int x;
    private int y;
    private int z;


    @Override
    public void read(ByteBuf buf, Version version) {
        if ( version.isAfterOrEq(Version.MINECRAFT_1_13) )
        {
            transactionId = readVarInt( buf );
        }
        cursor = readString( buf );

        if ( version.isBefore(Version.MINECRAFT_1_13) )
        {
            if ( version.isAfterOrEq(Version.MINECRAFT_1_9) )
            {
                assumeCommand = buf.readBoolean();
            }

            if ( hasPositon = buf.readBoolean() )
            {
                long position = buf.readLong();
                x = Position.getX(position, version);
                y = Position.getY(position, version);
                z = Position.getZ(position, version);
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if ( version.isAfterOrEq(Version.MINECRAFT_1_13) )
        {
            writeVarInt( transactionId, buf );
        }
        writeString( cursor, buf );

        if ( version.isBefore(Version.MINECRAFT_1_13) )
        {
            if ( version.isAfterOrEq(Version.MINECRAFT_1_9) )
            {
                buf.writeBoolean( assumeCommand );
            }

            buf.writeBoolean( hasPositon );
            if ( hasPositon )
            {
                buf.writeLong(Position.getPosition(x, y, z, version));
            }
        }
    }
}
