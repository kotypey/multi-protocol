package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
    public class InHeldItemChangePacket implements Packet {
    public short slot;

    @Override
    public void read(ByteBuf buf, Version version) {
        slot = buf.readShort();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeShort(slot);
    }
}
