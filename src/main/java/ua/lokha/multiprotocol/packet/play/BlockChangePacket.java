package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.BlockProtocol;
import ua.lokha.multiprotocol.type.Position;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
@Log
public class BlockChangePacket implements Packet {

    private int x;
    private int y;
    private int z;
    public String blockType;

    @Override
    public void read(ByteBuf buf, Version version) {
        long position = buf.readLong();
        x = Position.getX(position, version);
        y = Position.getY(position, version);
        z = Position.getZ(position, version);

        int id = readVarInt(buf);
        blockType = BlockProtocol.BLOCK_PROTOCOL.getValue(id, version);
        if (blockType == null) {
            log.warning("blockType not found by id " + id + " and version " + version);
            blockType = BlockProtocol.AIR;
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(Position.getPosition(x, y, z, version));
        int id = BlockProtocol.BLOCK_PROTOCOL.getId(blockType, version);
        if (id == -1) {
            log.warning("id not found by blockType " + blockType + " and version " + version);
            id = 0;
        }
        writeVarInt(id, buf);
    }
}
