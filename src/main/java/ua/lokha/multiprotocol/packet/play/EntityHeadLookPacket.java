package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class EntityHeadLookPacket implements Packet {
    private int entityId;
    private byte headYaw;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        headYaw = buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        buf.writeByte(headYaw);
    }
}
