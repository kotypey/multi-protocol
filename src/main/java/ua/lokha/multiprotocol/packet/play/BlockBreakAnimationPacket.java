package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Position;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class BlockBreakAnimationPacket implements Packet {

    private int entityId;
    private int x;
    private int y;
    private int z;
    private byte destroyStage;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = PacketUtils.readVarInt(buf);

        long position = buf.readLong();
        x = Position.getX(position, version);
        y = Position.getY(position, version);
        z = Position.getZ(position, version);

        destroyStage = buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(entityId, buf);
        buf.writeLong(Position.getPosition(x, y, z, version));
        buf.writeByte(destroyStage);
    }
}
