package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class OutPlayerPositionAndLookPacket implements Packet {
    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;
    private byte flags;
    private int teleportId;

    @Override
    public void read(ByteBuf buf, Version version) {
        x = buf.readDouble();
        y = buf.readDouble();
        z = buf.readDouble();
        yaw = buf.readFloat();
        pitch = buf.readFloat();
        flags = buf.readByte();
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            teleportId = PacketUtils.readVarInt(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeDouble(x);
        buf.writeDouble(y);
        buf.writeDouble(z);
        buf.writeFloat(yaw);
        buf.writeFloat(pitch);
        buf.writeByte(flags);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            PacketUtils.writeVarInt(teleportId, buf);
        }
    }
}
