package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class PlayerPacket implements Packet {
    private boolean onGround;

    @Override
    public void read(ByteBuf buf, Version version) {
        onGround = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(onGround);
    }
}
