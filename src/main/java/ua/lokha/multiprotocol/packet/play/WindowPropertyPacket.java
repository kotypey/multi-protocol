package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
//todo: доделать remap
public class WindowPropertyPacket implements Packet {

    private short windowId;
    private short property;
    private short value;

    @Override
    public void read(ByteBuf buf, Version version) {
        windowId = buf.readUnsignedByte();
        property = buf.readShort();
        value = buf.readShort();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(windowId);
        buf.writeShort(property);
        buf.writeShort(value);
    }
}
