package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class UseEntityPacket implements Packet {
    private int target;
    private int type;
    private float x;
    private float y;
    private float z;
    private int hand;

    @Override
    public void read(ByteBuf buf, Version version) {
        target = readVarInt(buf);
        type = readVarInt(buf);
        if (type == 2) {
            x = buf.readFloat();
            y = buf.readFloat();
            z = buf.readFloat();
        }
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            if (type == 0 || type == 2) {
                hand = readVarInt(buf);
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(target, buf);
        writeVarInt(type, buf);
        if (type == 2) {
            buf.writeFloat(x);
            buf.writeFloat(y);
            buf.writeFloat(z);
        }
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            if (type == 0 || type == 2) {
                writeVarInt(hand, buf);
            }
        }
    }
}
