package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.nbt.Tag;

import java.util.List;
import java.util.Map;

import static ua.lokha.multiprotocol.util.PacketUtils.*;


/**
 * Сделал как мог, если нужно что-то подправить см. доки https://wiki.vg/index.php?title=Protocol&oldid=14204#Advancements
 */
@Data
public class AdvancementsPacket implements Packet {

    private boolean reset;
    private int mappingSize;
    private AdvancementMapping[] advancementMapping;
    private int listSize;
    private String[] identifiers;
    private int progressSize;
    private ProgressMapping[] progressMapping;

    @Override
    public void read(ByteBuf buf, Version version) {
        reset = buf.readBoolean();
        mappingSize = readVarInt(buf);
        advancementMapping = new  AdvancementMapping[mappingSize];
        for(int i = 0; i < mappingSize; i++){
            advancementMapping[i] = new AdvancementMapping();
            advancementMapping[i].read(buf, version);

        }

        listSize = readVarInt(buf);
        identifiers = new String[listSize];
        for(int i = 0; i < listSize; i++){
            identifiers[i] = readString(buf);
        }
        progressSize = readVarInt(buf);
        progressMapping = new ProgressMapping[progressSize];
        for(int i = 0; i < progressSize; i++){
            progressMapping[i] = new ProgressMapping();
            progressMapping[i].read(buf, version);
        }
    }


    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(reset);
        mappingSize = advancementMapping.length;
        writeVarInt(mappingSize, buf);
        for(AdvancementMapping mapping : advancementMapping){
            mapping.write(buf, version);
        }
        listSize = identifiers.length;
        writeVarInt(listSize, buf);

        for(String str : identifiers){
            writeString(str, buf);
        }
        progressSize = progressMapping.length;
        for(ProgressMapping mapping : progressMapping){
            mapping.write(buf,version);
        }
    }



}

@Data
class ProgressMapping{
    private String id;
    private AdvancementProgress progress;

    public void read(ByteBuf buf, Version version){
        id = readString(buf);
        progress = new AdvancementProgress();
        progress.read(buf, version);
    }


    public void write(ByteBuf buf, Version version){
        writeString(id, buf);
        progress.write(buf, version);
    }
}
@Data
class AdvancementMapping{
    private String identifier;
    private Advancement advancement;


    public void read(ByteBuf buf, Version version){
        identifier = readString(buf);
        advancement = new Advancement();
        advancement.read(buf, version);
    }

    public void write(ByteBuf buf, Version version){
        writeString(identifier, buf);
        advancement.write(buf, version);

    }
}


@Data
class Advancement{

    private boolean hasParent;
    private String optionalId;
    private boolean hasDisplay;
    private AdvancementDisplay optionalDisplay;
    private int numberOfCriteria;
    private Criterias[] criteria;
    private int arrayLenght;
    private Requirements[] requirements;

    public void read(ByteBuf buf, Version version) {
        hasParent = buf.readBoolean();
        if(hasParent){
            optionalId = readString(buf);
        }
        hasDisplay = buf.readBoolean();
        if(hasDisplay){
            optionalDisplay = new AdvancementDisplay();
            optionalDisplay.read(buf, version);
        }
        numberOfCriteria = readVarInt(buf);
        criteria = new Criterias[numberOfCriteria];
        for(int i = 0; i < numberOfCriteria; i++){
            criteria[i] = new Criterias();
            criteria[i].read(buf,version);
        }
        arrayLenght = readVarInt(buf);
        requirements = new Requirements[arrayLenght];
        for(int i1 = 0; i1 < arrayLenght; i1++){
            requirements[i1] = new Requirements();
            requirements[i1].read(buf,version);
        }
    }

    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(hasParent);
        if(hasParent){
            writeString(optionalId, buf);
        }
        hasDisplay = buf.readBoolean();
        if(hasDisplay){
            optionalDisplay.write(buf, version);
        }
        numberOfCriteria = criteria.length;
        writeVarInt(numberOfCriteria, buf);
        for(Criterias criterias : criteria){
            criterias.write(buf,version);
        }
        arrayLenght = requirements.length;
        writeVarInt(arrayLenght, buf);
        for(Requirements req : requirements){
            req.write(buf, version);
        }

    }
}

@Data
class AdvancementDisplay{


    private String title;
    private String description;
    private Slot slot;
    private int frameType;
    private int flags;
    private String backgroundid;
    private float xCoord;
    private float yCoord;


    public void read(ByteBuf buf, Version version){
        title = readString(buf);
        description = readString(buf);
        slot = new Slot();
        slot.read(buf, version);
        frameType = readVarInt(buf);
        flags = buf.readInt();
        if(((flags >> 0x1) & 1) > 0){
            backgroundid = readString(buf);

        }

        xCoord = buf.readFloat();
        yCoord = buf.readFloat();
    }


    public void write(ByteBuf buf, Version version){
        writeString(title, buf);
        writeString(description, buf);
        slot.write(buf, version);
        writeVarInt(frameType, buf);
        buf.writeInt(flags);
        if(((flags >> 0x1) & 1) > 0){
            writeString(backgroundid, buf);
        }

        buf.writeFloat(xCoord);
        buf.writeFloat(yCoord);

    }

}



@Data
class Slot{
    private boolean present;
    private int itemID;
    private byte itemCount;
    private Tag NBT;

    public void read(ByteBuf buf, Version version){
        present = buf.readBoolean();
        if(present){
            itemID = readVarInt(buf);
            itemCount = buf.readByte();
            NBT = Tag.readNamedTag(buf);
        }
    }


    public void write(ByteBuf buf, Version version){
        buf.writeBoolean(present);
        if(present){
            writeVarInt(itemID, buf);
            buf.writeByte(itemCount);
            Tag.writeNamedTag(NBT, buf);

        }
    }


    }
@Data
class AdvancementProgress{
    private int size;
    private Criteria[] criteria;


    public void read(ByteBuf buf, Version version){
        size = readVarInt(buf);
        criteria = new Criteria[size];
        for(int i = 0; i < size; i++){
            criteria[i] = new Criteria();
            criteria[i].read(buf, version);
        }
    }

    public void write(ByteBuf buf, Version version){
        size = criteria.length;
        writeVarInt(size, buf);

        for(Criteria criteria : criteria){
            criteria.write(buf, version);
        }

    }
}


@Data
class Criteria{
    private String id;
    private CriterionProgress progress;


    public void read(ByteBuf buf, Version version){
        id = readString(buf);
        progress = new CriterionProgress();
        progress.read(buf, version);
    }

    public void write(ByteBuf buf, Version version){
        writeString(id, buf);
        progress.write(buf, version);
    }
}



@Data
class CriterionProgress{
    private boolean achieved;
    private long dateOfAchieving;

    public void read(ByteBuf buf, Version version){
        achieved = buf.readBoolean();
        if(achieved){
            dateOfAchieving = buf.readLong();
        }
    }



    public void write(ByteBuf buf, Version version){
        buf.writeBoolean(achieved);
        if(achieved){
            buf.writeLong(dateOfAchieving);
        }

    }

}

/**
 * public static void serializeToNetwork(Map<String, Criterion> criteria, PacketBuffer buf) {
 *       buf.writeVarInt(criteria.size());
 *
 *       for(Entry<String, Criterion> entry : criteria.entrySet()) {
 *          buf.writeString(entry.getKey());
 *          entry.getValue().serializeToNetwork(buf);
 *       }
 *
 *    }
 */
@Data
class Criterias{
    private String key;
    private Criteria entry;


    public void read(ByteBuf buf, Version version){
        key = readString(buf);
        entry = new Criteria();
        entry.read(buf, version);
    }

    public void write(ByteBuf buf, Version version){
        writeString(key, buf);
        entry.write(buf, version);
    }


}


@Data
class Requirements{
    private int arrayLength;
    private String[] array;

    public void read(ByteBuf buf, Version version){
        arrayLength = readVarInt(buf);
        array = new String[arrayLength];
        for(int i = 0; i < arrayLength; i++){
            array[i] = readString(buf);
        }

    }

    public void write(ByteBuf buf, Version version){
        arrayLength = array.length;
        writeVarInt(arrayLength, buf);
        for(String str : array){
            writeString(str, buf);
        }
    }
}
