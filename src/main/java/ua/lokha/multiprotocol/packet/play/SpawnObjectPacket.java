package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.RemapConventions;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.EntityProtocol;
import ua.lokha.multiprotocol.type.EntityType;

import java.util.UUID;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class SpawnObjectPacket implements Packet {

    private int entityId;
    private UUID uuid;
    private EntityType type;
    private double x;
    private double y;
    private double z;
    private byte pitch;
    private byte yaw;
    private int data;
    private short velocityX;
    private short velocityY;
    private short velocityZ;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            uuid = readUUID(buf);
        }
        int typeId;
        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            typeId = readVarInt(buf);
        } else {
            typeId = buf.readByte();
        }
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            x = buf.readDouble();
            y = buf.readDouble();
            z = buf.readDouble();
        } else {
            x = buf.readInt() / 32.0D;
            y = buf.readInt() / 32.0D;
            z = buf.readInt() / 32.0D;
        }
        pitch = buf.readByte();
        yaw = buf.readByte();
        data = buf.readInt();
        type = EntityProtocol.getByObjectId(typeId, data, version);
        if (data != 0 || version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            velocityX = buf.readShort();
            velocityY = buf.readShort();
            velocityZ = buf.readShort();
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            writeUUID(uuid != null ? uuid : RemapConventions.entityIdToUUID(entityId), buf);
        }
        int typeId = EntityProtocol.OBJECT_PROTOCOL.getId(type, version);
        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            writeVarInt(typeId, buf);
        } else {
            buf.writeByte(typeId);
        }
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            buf.writeDouble(x);
            buf.writeDouble(y);
            buf.writeDouble(z);
        } else {
            buf.writeInt((int) (x * 32.0D));
            buf.writeInt((int) (y * 32.0D));
            buf.writeInt((int) (z * 32.0D));
        }
        buf.writeByte(pitch);
        buf.writeByte(yaw);
        buf.writeInt(data);
        if (data != 0 || version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            buf.writeShort(velocityX);
            buf.writeShort(velocityY);
            buf.writeShort(velocityZ);
        }
    }
}
