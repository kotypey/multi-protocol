package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class SetExperiencePacket implements Packet {
    private float experienceBar;
    private int level;
    private int totalExperience;

    @Override
    public void read(ByteBuf buf, Version version) {
        experienceBar = buf.readFloat();
        level = readVarInt(buf);
        totalExperience = readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeFloat(experienceBar);
        writeVarInt(level, buf);
        writeVarInt(totalExperience, buf);
    }
}
