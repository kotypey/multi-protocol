package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class InPlayerAbilitiesPacket implements Packet {

    private byte flags;
    private float flyingSpeed;
    private float walkingSpeed;

    @Override
    public void read(ByteBuf buf, Version version) {
        flags = buf.readByte();
        flyingSpeed = buf.readFloat();
        walkingSpeed = buf.readFloat();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(flags);
        buf.writeFloat(flyingSpeed);
        buf.writeFloat(walkingSpeed);
    }
}
