package ua.lokha.multiprotocol.packet.login;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

/**
 * Это пакет из протокола банги
 */
@Data
public class LoginPayloadResponsePacket implements Packet {
    private int id;
    private byte[] data;

    @Override
    public void read(ByteBuf buf, Version version) {
        this.id = readVarInt(buf);
        if (buf.readBoolean()) {
            int len = buf.readableBytes();
            if (len > 1048576) {
                throw new IllegalStateException("Payload may not be larger than 1048576 bytes");
            }

            this.data = new byte[len];
            buf.readBytes(this.data);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(this.id, buf);
        if (this.data != null) {
            buf.writeBoolean(true);
            buf.writeBytes(this.data);
        } else {
            buf.writeBoolean(false);
        }
    }
}
