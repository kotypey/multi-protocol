package ua.lokha.multiprotocol.packet.login;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readString;
import static ua.lokha.multiprotocol.util.PacketUtils.writeString;

@Data
public class LoginSuccessPacket implements Packet {
    private String uuid;
    private String username;

    @Override
    public void read(ByteBuf buf, Version version) {
        this.uuid = readString(buf);
        this.username = readString(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeString(this.uuid, buf);
        writeString(this.username, buf);
    }
}
