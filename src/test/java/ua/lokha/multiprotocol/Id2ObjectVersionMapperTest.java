package ua.lokha.multiprotocol;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static ua.lokha.multiprotocol.Id2ObjectVersionMapper.map;

public class Id2ObjectVersionMapperTest {

    @Test
    public void test() {
        Id2ObjectVersionMapper<String> mapper = new Id2ObjectVersionMapper<>();

        mapper.register("value1",
                map(Version.MINECRAFT_1_9, 10),
                map(Version.MINECRAFT_1_11, 20));

        mapper.register("value2",
                map(Version.MINECRAFT_1_10, 30),
                map(Version.MINECRAFT_1_13_1, 10));

        for (Version version : Version.getVersions()) {
            assertNull(mapper.getValue(34324 /* any no used id */, version));
            assertEquals(-1, mapper.getId("value3", version));
        }

        assertEquals(-1, mapper.getId("value1", Version.MINECRAFT_1_8));
        assertEquals(10, mapper.getId("value1", Version.MINECRAFT_1_9));
        assertEquals(10, mapper.getId("value1", Version.MINECRAFT_1_10));
        assertEquals(20, mapper.getId("value1", Version.MINECRAFT_1_11));
        assertEquals(20, mapper.getId("value1", Version.MINECRAFT_1_12_1));

        assertEquals(-1, mapper.getId("value2", Version.MINECRAFT_1_8));
        assertEquals(-1, mapper.getId("value2", Version.MINECRAFT_1_9));
        assertEquals(30, mapper.getId("value2", Version.MINECRAFT_1_10));
        assertEquals(30, mapper.getId("value2", Version.MINECRAFT_1_11));
        assertEquals(30, mapper.getId("value2", Version.MINECRAFT_1_12_1));
        assertEquals(10, mapper.getId("value2", Version.MINECRAFT_1_13_1));
        assertEquals(10, mapper.getId("value2", Version.MINECRAFT_1_14));

        assertEquals("value1", mapper.getValue(10, Version.MINECRAFT_1_9));
        assertEquals("value1", mapper.getValue(10, Version.MINECRAFT_1_10));
        assertEquals("value1", mapper.getValue(20, Version.MINECRAFT_1_11));
        assertEquals("value1", mapper.getValue(20, Version.MINECRAFT_1_12_1));

        assertEquals("value2", mapper.getValue(30, Version.MINECRAFT_1_10));
        assertEquals("value2", mapper.getValue(30, Version.MINECRAFT_1_11));
        assertEquals("value2", mapper.getValue(10, Version.MINECRAFT_1_13_1));
        assertEquals("value2", mapper.getValue(10, Version.MINECRAFT_1_14));
    }
}