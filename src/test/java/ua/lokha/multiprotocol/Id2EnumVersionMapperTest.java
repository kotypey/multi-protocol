package ua.lokha.multiprotocol;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static ua.lokha.multiprotocol.Id2EnumVersionMapper.map;

public class Id2EnumVersionMapperTest {

    @Test
    public void test() {
        Id2EnumVersionMapper<TestEnum> mapper = new Id2EnumVersionMapper<>(TestEnum.class);

        mapper.register(TestEnum.VALUE1,
                map(Version.MINECRAFT_1_9, 10),
                map(Version.MINECRAFT_1_11, 20));

        mapper.register(TestEnum.VALUE2,
                map(Version.MINECRAFT_1_10, 30),
                map(Version.MINECRAFT_1_13_1, 10));

        for (Version version : Version.getVersions()) {
            assertNull(mapper.getEnum(34324 /* any no used id */, version));
            assertEquals(-1, mapper.getId(TestEnum.VALUE3, version));
        }

        assertEquals(-1, mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_8));
        assertEquals(10, mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_9));
        assertEquals(10, mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_10));
        assertEquals(20, mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_11));
        assertEquals(20, mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_12_1));

        assertEquals(-1, mapper.getId(TestEnum.VALUE2, Version.MINECRAFT_1_8));
        assertEquals(-1, mapper.getId(TestEnum.VALUE2, Version.MINECRAFT_1_9));
        assertEquals(30, mapper.getId(TestEnum.VALUE2, Version.MINECRAFT_1_10));
        assertEquals(30, mapper.getId(TestEnum.VALUE2, Version.MINECRAFT_1_11));
        assertEquals(30, mapper.getId(TestEnum.VALUE2, Version.MINECRAFT_1_12_1));
        assertEquals(10, mapper.getId(TestEnum.VALUE2, Version.MINECRAFT_1_13_1));
        assertEquals(10, mapper.getId(TestEnum.VALUE2, Version.MINECRAFT_1_14));

        assertEquals(TestEnum.VALUE1, mapper.getEnum(10, Version.MINECRAFT_1_9));
        assertEquals(TestEnum.VALUE1, mapper.getEnum(10, Version.MINECRAFT_1_10));
        assertEquals(TestEnum.VALUE1, mapper.getEnum(20, Version.MINECRAFT_1_11));
        assertEquals(TestEnum.VALUE1, mapper.getEnum(20, Version.MINECRAFT_1_12_1));

        assertEquals(TestEnum.VALUE2, mapper.getEnum(30, Version.MINECRAFT_1_10));
        assertEquals(TestEnum.VALUE2, mapper.getEnum(30, Version.MINECRAFT_1_11));
        assertEquals(TestEnum.VALUE2, mapper.getEnum(10, Version.MINECRAFT_1_13_1));
        assertEquals(TestEnum.VALUE2, mapper.getEnum(10, Version.MINECRAFT_1_14));
    }

    public enum TestEnum {
        VALUE1,
        VALUE2,
        VALUE3,
        ;
    }
}