import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.SneakyThrows;
import lombok.var;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import ua.lokha.multiprotocol.Id2EnumVersionMapper;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.EntityType;
import ua.lokha.multiprotocol.util.ResourceUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class MobProtocolPopulate {

    public static void main(String[] args) throws Exception {
        Gson gson = new Gson();
        Id2EnumVersionMapper<EntityType> MOB_PROTOCOL = loadFromResource("mob-protocol.json", EntityType.class);
        JsonArray mobs = gson.fromJson(IOUtils.toString(MobProtocol1_12Populate.class.getResourceAsStream("mob-protocol.json")), JsonArray.class);

        String viaMob1_13 = IOUtils.toString(MobProtocolPopulate.class.getResourceAsStream("via-mob-1.14.txt"));
        String viaMob1_14 = IOUtils.toString(MobProtocolPopulate.class.getResourceAsStream("via-mob-1.15.txt"));

        Map<Integer, String> v_1_13 = new HashMap<>();
        Map<String, Integer> v_1_14 = new HashMap<>();

        for (String line : viaMob1_13.split("\n")) {
            try {
                String[] data = line.trim().split("\\(");
                v_1_13.put(Integer.parseInt(data[1]), data[0]);
            } catch (Exception e) {
                System.out.println("error : " + line);
                e.printStackTrace();
            }
        }

        for (String line : viaMob1_14.split("\n")) {
            try {
                String[] data = line.trim().split("\\(");
                v_1_14.put(data[0], Integer.parseInt(data[1]));
            } catch (Exception e) {
                System.out.println("error : " + line);
                e.printStackTrace();
            }
        }

        for (JsonElement mob : mobs) {
            JsonObject object = mob.getAsJsonObject();
            int get_1_13 = MOB_PROTOCOL.getId(EntityType.valueOf(object.get("enum").getAsString()), Version.MINECRAFT_1_14);
            if (get_1_13 != -1) {
                String name1_13 = v_1_13.get(get_1_13);
                if (name1_13 != null) {
                    Integer id1_14 = v_1_14.get(name1_13);
                    if (id1_14 != null) {
                        JsonObject map = new JsonObject();
                        map.addProperty("id", id1_14);
                        map.addProperty("version", "MINECRAFT_1_15");
                        object.get("map").getAsJsonArray().add(map);
                    }
                }
            }
        }

        String json = gson.toJson(mobs);
        FileUtils.writeStringToFile(new File("out.txt"), json);
    }

    @SneakyThrows
    public static <T extends Enum<T>> Id2EnumVersionMapper<T> loadFromResource(String resourceName, Class<T> enumClass) {
        Id2EnumVersionMapper<T> mapper = new Id2EnumVersionMapper<>(enumClass);

        JsonArray blocks = ResourceUtils.GSON.fromJson(IOUtils.toString(MobProtocol1_12Populate.class.getResourceAsStream(resourceName)), JsonArray.class);
        for (JsonElement element : blocks) {
            try {
                JsonObject block = element.getAsJsonObject();
                T enumValue = Enum.valueOf(enumClass, block.get("enum").getAsString());

                JsonArray mapList = block.get("map").getAsJsonArray();
                var idMappers = new Id2EnumVersionMapper.IdMapper[mapList.size()];
                for (int i = 0; i < mapList.size(); i++) {
                    JsonObject mapItem = mapList.get(i).getAsJsonObject();
                    Version version = Version.valueOf(mapItem.get("version").getAsString());
                    int id = mapItem.get("id").getAsInt();
                    idMappers[i] = Id2EnumVersionMapper.map(version, id);
                }

                mapper.register(enumValue, idMappers);
            } catch (Exception e) {
                System.out.println("Ошибка загрузки " + resourceName + ": " + element);
                e.printStackTrace();
            }
        }
        return mapper;
    }
}
