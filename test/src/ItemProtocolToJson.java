import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemProtocolToJson {
    public static void main(String[] args) throws Exception {
        Gson gson = new Gson();
        List list = gson.fromJson(IOUtils.toString(MobProtocol1_12Populate.class.getResourceAsStream("items1.13.json")), List.class);

        String currentProtocol = IOUtils.toString(MobProtocol1_12Populate.class.getResourceAsStream("current-protocol-with-item.txt")).replace("\r", "");

        Map<String, String> populate = new HashMap<>();
        for (Object o : list) {
            Map map = (Map) o;
            int id = ((Number) map.get("id")).intValue();
            String name = (String) map.get("name");
            populate.put(name, String.valueOf(id));
        }

        JsonArray result = new JsonArray();

        String[] current = currentProtocol.split("\n");
        for (int i = 0; i < current.length; i++) {
            String line = current[i];

            String materialName = StringUtils.substringBetween(line, ".register(Material.", ", map").trim();
            String oldId = StringUtils.substringBetween(line, "MINECRAFT_1_8, ", ")").trim();
            String newId = populate.get(materialName.toLowerCase());

            JsonObject item = new JsonObject();
            item.addProperty("enum", materialName);

            JsonArray versions = new JsonArray();

            {
                JsonObject v1_8 = new JsonObject();
                v1_8.addProperty("version", "MINECRAFT_1_8");
                v1_8.addProperty("id", Integer.parseInt(oldId));
                versions.add(v1_8);
            }

            {
                JsonObject v1_13 = new JsonObject();
                v1_13.addProperty("version", "MINECRAFT_1_13");
                v1_13.addProperty("id", newId == null ? -1 : Integer.parseInt(newId));
                versions.add(v1_13);
            }

            item.add("map", versions);

            result.add(item);
        }


        String json = gson.toJson(result);

        FileUtils.writeStringToFile(new File("out.txt"), json);
    }
}
