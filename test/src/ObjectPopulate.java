import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.SneakyThrows;
import lombok.var;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import ua.lokha.multiprotocol.Id2EnumVersionMapper;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.EntityType;
import ua.lokha.multiprotocol.util.ResourceUtils;

import java.io.File;

public class ObjectPopulate {

    public static void main(String[] args) throws Exception {

        Gson gson = new Gson();
        JsonArray objects = gson.fromJson(IOUtils.toString(MobProtocol1_12Populate.class.getResourceAsStream("object-protocol.json")), JsonArray.class);

        Id2EnumVersionMapper<EntityType> MOB_PROTOCOL = loadFromResource("mob-protocol.json", EntityType.class);
        Id2EnumVersionMapper<EntityType> OBJECT_PROTOCOL = loadFromResource("object-protocol.json", EntityType.class);

        for (JsonElement object : objects) {
            JsonObject jsonObject = object.getAsJsonObject();
            EntityType entityType = EntityType.valueOf(jsonObject.get("enum").getAsString());

            int mobId = MOB_PROTOCOL.getId(entityType, Version.MINECRAFT_1_14);
            int objectId = OBJECT_PROTOCOL.getId(entityType, Version.MINECRAFT_1_14);

            if (mobId != objectId) {
                JsonObject map = new JsonObject();
                map.addProperty("id", mobId);
                map.addProperty("version", "MINECRAFT_1_14");
                jsonObject.get("map").getAsJsonArray().add(map);
            }
        }

        String json = gson.toJson(objects);

        FileUtils.writeStringToFile(new File("out.txt"), json);
    }

    @SneakyThrows
    public static <T extends Enum<T>> Id2EnumVersionMapper<T> loadFromResource(String resourceName, Class<T> enumClass) {
        Id2EnumVersionMapper<T> mapper = new Id2EnumVersionMapper<>(enumClass);

        JsonArray blocks = ResourceUtils.GSON.fromJson(IOUtils.toString(MobProtocol1_12Populate.class.getResourceAsStream(resourceName)), JsonArray.class);
        for (JsonElement element : blocks) {
            try {
                JsonObject block = element.getAsJsonObject();
                T enumValue = Enum.valueOf(enumClass, block.get("enum").getAsString());

                JsonArray mapList = block.get("map").getAsJsonArray();
                var idMappers = new Id2EnumVersionMapper.IdMapper[mapList.size()];
                for (int i = 0; i < mapList.size(); i++) {
                    JsonObject mapItem = mapList.get(i).getAsJsonObject();
                    Version version = Version.valueOf(mapItem.get("version").getAsString());
                    int id = mapItem.get("id").getAsInt();
                    idMappers[i] = Id2EnumVersionMapper.map(version, id);
                }

                mapper.register(enumValue, idMappers);
            } catch (Exception e) {
                System.out.println("Ошибка загрузки " + resourceName + ": " + element);
                e.printStackTrace();
            }
        }
        return mapper;
    }
}
